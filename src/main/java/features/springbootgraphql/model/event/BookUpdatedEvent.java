package features.springbootgraphql.model.event;

import javax.validation.constraints.NotNull;

import features.springbootgraphql.model.Book;

public class BookUpdatedEvent {
    @NotNull
    String isn;
    @NotNull
    String title;
    @NotNull
    String publisher;
    @NotNull
    String[] authors;
    @NotNull
    String publishedDate;

    public BookUpdatedEvent(@NotNull Book book) {
        this.isn = book.getIsn();
        this.title = book.getTitle();
        this.publisher = book.getPublisher();
        this.authors = book.getAuthors();
        this.publishedDate = book.getPublishedDate();

    }
}
