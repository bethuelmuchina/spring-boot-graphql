package features.springbootgraphql.model.event;

import javax.validation.constraints.NotNull;

import features.springbootgraphql.model.Book;
import lombok.Value;

@Value
public class BookCreatedEvent {
    @NotNull
    String isn;
    @NotNull
    String title;
    @NotNull
    String publisher;
    @NotNull
    String[] authors;
    @NotNull
    String publishedDate;

    public BookCreatedEvent(@NotNull Book book) {
        this.isn = book.getIsn();
        this.title = book.getTitle();
        this.publisher = book.getPublisher();
        this.authors = book.getAuthors();
        this.publishedDate = book.getPublishedDate();

    }
}
