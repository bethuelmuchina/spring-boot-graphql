package features.springbootgraphql.model.event;

import javax.validation.constraints.NotNull;

public class BookDeletedEvent {
    @NotNull
    String isn;

    public BookDeletedEvent(@NotNull String isn) {
        this.isn = isn;

    }
}
