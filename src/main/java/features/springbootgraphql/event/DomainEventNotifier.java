package features.springbootgraphql.event;

import lombok.Getter;

@Getter
public class DomainEventNotifier {

    private static DomainEventNotifier instance;
    private final EventPublisher eventPublisher;

    public DomainEventNotifier() {
        this.eventPublisher = new TransactionalEventPublisherImpl(EventPublisherFactory.getInstance().getEventPublisher());
    }

    private static DomainEventNotifier getInstance() {
        if (instance == null) {
            instance = new DomainEventNotifier();
        }
        return instance;
    }

    public static void notifyEvent(Object event) {
        getInstance().getEventPublisher().publishEvent(event);
    }

}
