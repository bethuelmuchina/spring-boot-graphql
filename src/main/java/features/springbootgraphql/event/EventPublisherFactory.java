package features.springbootgraphql.event;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EventPublisherFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventPublisherFactory.class);

    private static EventPublisherFactory instance;
    private final ApplicationEventPublisher eventPublisher;

    public EventPublisherFactory(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public static EventPublisherFactory getInstance() {
        if (instance == null) {
            // IF is requested outside spring context
            instance = new EventPublisherFactory(event ->
                    // DO NOTHING;
                    LOGGER.debug("A message was received, but it is not on a spring context. No event was send.")
            );
        }
        return instance;
    }

    @PostConstruct
    private void postConstruct() {
        instance = this;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }
}
