package features.springbootgraphql.event;

public interface EventPublisher {

    void publishEvent(Object event);

}
