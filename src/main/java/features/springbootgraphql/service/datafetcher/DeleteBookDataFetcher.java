package features.springbootgraphql.service.datafetcher;

import features.springbootgraphql.repository.BookRepository;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteBookDataFetcher {

    @Autowired
    BookRepository bookRepository;

    public DataFetcher<Boolean> deleteBook() {
        return dataFetchingEnvironment -> {
            String isn = dataFetchingEnvironment.getArgument("id");
            bookRepository.deleteById(isn);
            return true;
        };
    }
}
