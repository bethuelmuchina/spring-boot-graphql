package features.springbootgraphql.service.datafetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import features.springbootgraphql.model.Book;
import features.springbootgraphql.repository.BookRepository;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateBookDataFetcher {

    @Autowired
    BookRepository bookRepository;

    public DataFetcher<Book> updateBook() {
        return dataFetchingEnvironment -> {
            String isn = dataFetchingEnvironment.getArgument("isn");
            Optional<Book> optBook = bookRepository.findById(isn);
            String title = dataFetchingEnvironment.getArgument("title");
            String publisher = dataFetchingEnvironment.getArgument("publisher");
            List<String> authors = new ArrayList<>(dataFetchingEnvironment.getArgument("authors"));
            String[] stringArray = authors.stream().toArray(String[]::new);
            String publishedDate = dataFetchingEnvironment.getArgument("publishedDate");

            if (optBook.isPresent()) {
                Book book = optBook.get();

                if (title != null) {
                    book.setTitle(title);
                }
                if (publisher != null) {
                    book.setPublisher(publisher);
                }
                if (stringArray != null) {
                    book.setAuthors(stringArray);
                }
                if (publishedDate != null) {
                    book.setPublishedDate(publishedDate);
                }
                return bookRepository.save(book);

            } else {
                return null;
            }
        };
    }
}

