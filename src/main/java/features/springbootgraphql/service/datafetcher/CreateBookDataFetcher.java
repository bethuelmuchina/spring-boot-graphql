package features.springbootgraphql.service.datafetcher;

import java.util.ArrayList;
import java.util.List;

import features.springbootgraphql.model.Book;
import features.springbootgraphql.repository.BookRepository;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateBookDataFetcher {

    @Autowired
    BookRepository bookRepository;

    public DataFetcher<Book> createBook() {
        return dataFetchingEnvironment -> {
            String isn = dataFetchingEnvironment.getArgument("isn");
            String title = dataFetchingEnvironment.getArgument("title");
            String publisher = dataFetchingEnvironment.getArgument("publisher");
            List<String> authors = new ArrayList<>(dataFetchingEnvironment.getArgument("authors"));
            String[] stringArray = authors.stream().toArray(String[]::new);
            String publishedDate = dataFetchingEnvironment.getArgument("publishedDate");

            Book book = new Book();
            book.setIsn(isn);
            book.setTitle(title);
            book.setPublisher(publisher);
            book.setAuthors(stringArray);
            book.setPublishedDate(publishedDate);
            return bookRepository.save(book);

        };
    }
}
