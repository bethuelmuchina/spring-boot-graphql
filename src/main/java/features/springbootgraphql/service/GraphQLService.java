package features.springbootgraphql.service;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;

import features.springbootgraphql.model.Book;
import features.springbootgraphql.repository.BookRepository;
import features.springbootgraphql.service.datafetcher.AllBooksDataFetcher;
import features.springbootgraphql.service.datafetcher.BookDataFetcher;
import features.springbootgraphql.service.datafetcher.CreateBookDataFetcher;
import features.springbootgraphql.service.datafetcher.DeleteBookDataFetcher;
import features.springbootgraphql.service.datafetcher.UpdateBookDataFetcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

@Service
public class GraphQLService {

    @Autowired
    BookRepository bookRepository;

    @Value("classpath:books.graphql")
    Resource resource;

    private GraphQL graphQL;
    @Autowired
    private AllBooksDataFetcher allBooksDataFetcher;
    @Autowired
    private BookDataFetcher bookDataFetcher;
    @Autowired
    private CreateBookDataFetcher createBookDataFetcher;
    @Autowired
    private UpdateBookDataFetcher updateBookDataFetcher;
    @Autowired
    private DeleteBookDataFetcher deleteBookDataFetcher;

    // load schema at application start up
    @PostConstruct
    private void loadSchema() throws IOException {

        //Load Books into the Book Repository
        loadDataIntoHSQL();

        // get the schema
        File schemaFile = resource.getFile();
        // parse schema
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private void loadDataIntoHSQL() {

        Stream.of(
                new Book("123", "Cloud Computing", "Kindle Edition",
                        new String[] {
                                "Chloe Aridjis"
                        }, "Nov 2017"),
                new Book("124", "AI", "Orielly",
                        new String[] {
                                "Peter", "Sam"
                        }, "Jan 2015"),
                new Book("125", "Java 9 Programming", "Orielly",
                        new String[] {
                                "Venkat", "Ram"
                        }, "Dec 2016")
        ).forEach(book -> bookRepository.save(book));
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type(newTypeWiring("Query")
                        .dataFetcher("allBooks", allBooksDataFetcher)
                        .dataFetcher("book", bookDataFetcher))
                .type(newTypeWiring("Mutation")
                        .dataFetcher("createBook", createBookDataFetcher.createBook())
                        .dataFetcher("updateBook", updateBookDataFetcher.updateBook())
                        .dataFetcher("deleteBook", deleteBookDataFetcher.deleteBook()))
                .build();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
