Feature: Test GraphQL endpoint
  Background:
    * url 'http://localhost:8091/rest/books'
  Scenario: fetch all books
    Given text query =
 """{
allBooks  {
isn
title
publisher
authors
publishedDate
}
}"""
         # Request the query
    And request  { query: '#(query)'}

        # Use method POST (GraphQL always uses POST)
    When method POST

        # Assert status code is OK
    Then status 200

  Scenario: Update Book
    Given text mutation =
 """mutation {
     updateBook (isn: "125",title: "1",publisher: "Orielly",publishedDate: "Dec 2018",  authors : ["Chloe Aridjis","arsenal"] ) {
         isn
         title
         publisher
         authors
         publishedDate
     }
}"""
         # Request the mutation
    And request  { mutation: '#(mutation)'}

        # Use method POST (GraphQL always uses POST)
    When method POST

        # Assert status code is OK
    Then status 200



