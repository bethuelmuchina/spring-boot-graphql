package feature;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

@RunWith(Karate.class)
@CucumberOptions(features = "classpath:feature")
public class karateRunner {

    private static final WireMockServer wireMockServer
            = new WireMockServer(WireMockConfiguration.options().port(8091));

    @BeforeClass
    public static void setUp() {
        wireMockServer.start();
        WireMock.configureFor("localhost", wireMockServer.port());

        stubFor(
                post(urlEqualTo("/rest/books"))
                        .withRequestBody(containing("allBooks"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")));

        stubFor(
                post(urlEqualTo("/rest/books"))
                        .withRequestBody(containing("updateBook"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")));

    }

    @AfterClass
    public static void tearDown() {
        wireMockServer.stop();
    }
}

